---
title: Installing carbonOS
description: System requirements and guide on how to install carbonOS on hardware or in a VM
os_version: "2022.2"
---

These instructions are a quick guide to getting carbonOS installed and ready
for testing.

#### System Requirements

- EFI firmware
- CPU new enough to support x86-64-v3 (generally, CPUs made after 2016 work) 

<div class="alert alert-warning" role="alert">
  <strong>NVIDIA support is still experimental!</strong>
  There are still a couple of known issues and rough edges around NVIDIA GPUs.
  Some fixes may need to be applied manually. This is being worked on.
</div>

#### Installing onto hardware

1. Download the [ISO Image](/download)
2. Write the ISO to a USB flash drive, using `dd`, your OS's disk utility,
   [balenaEtcher](https://www.balena.io/etcher/), or similar software
3. Boot your machine into the EFI firmware. Disable secure boot, and
   ensure that your USB drive has priority in the boot order
4. Boot your machine from the USB drive
5. Select "Try or Install carbonOS" from the menu. If you have at least 8GB of
   RAM, you can select the ramdisk alternative if necessary (i.e. if you need to
   remove the USB drive during installation)
6. Once the system boots, select your language and follow the on-screen
   instructions

#### Installing onto GNOME Boxes

1. Use GNOME Boxes from [Flathub](https://flathub.org/apps/details/org.gnome.Boxes)
2. Download the [ISO Image](/download)
3. Create a new virtual machine, and select the ISO image you downloaded
4. Boxes will tell you that it couldn't recognize the OS. In the list of
   OS options, select "Ubuntu 22.04 LTS"
5. On the next screen, check the "Enable EFI" option. carbonOS will not boot
   if this is disabled
6. Follow the on-screen instructions to finish creating the VM
7. Start the VM
8. Select "Try or Install carbonOS" from the menu
9. Once the system boots, select your language and follow the on-screen
   instructions

#### Installing onto VirtualBox

For the best experience & compatibility, I'd recommend testing carbonOS through
GNOME Boxes. Running carbonOS on VirtualBox is possible, but there are known
limitations:

- VirtualBox tends to drop you into an EFI shell instead of properly selecting
  carbonOS's bootloader
- VirtualBox's GPU driver has incompatabilities with hardware-accelerated
  mouse cursors, so you may experience strangeness (invisible cursors,
  upside-down cursors, etc)

If you want to try working around these issues, please join #carbonOS:matrix.org
and I'll help you get it installed. Then I'll write some proper instructions here

#### Dual-boot, custom partitions, etc

These setups are not officially supported by carbonOS. However, if you'd like
to use carbonOS in these scenarios anyway, you'll need to follow the
[advanced installation instructions](/support/installation-custom.html).
Be warned that these instructions assume you have the prerequisite knowledge
to manually manipulate Linux partitions, filesystems, and configuration.
